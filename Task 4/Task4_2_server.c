#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#define PORT 		1234
#define MAXLINE 	100000

int main() {
	int s_fd;
	char buf[MAXLINE];
	char buf1[MAXLINE];
	struct s_addr_in servaddr, cl_addr;
	printf("Hello\n");

	if ( (s_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	} 

	memset(&servaddr, 0, sizeof(servaddr));
	memset(&cl_addr, 0, sizeof(cl_addr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(PORT);

	if ( bind(s_fd, (const struct s_addr *)&servaddr, sizeof(servaddr)) < 0 )
	{ 
		perror("bind failed");
		exit(EXIT_FAILURE);
	}
	int len, n;
    len = sizeof(cl_addr);
    int cnt = 0;
    while(10){
    	if(cnt >= 10) break;
    	n = recvfrom(s_fd, (char *)buf, MAXLINE, MSG_WAITALL, ( struct s_addr *) &cl_addr, &len);
    	buf[n] = '\0';
    	sendto(s_fd, (const char *)buf1, n, MSG_CONFIRM, (const struct s_addr *) &cl_addr, len);
    	cnt++;
    }

	return 0;
} 