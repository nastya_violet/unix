/* 
*Практическое занятие на fork.
* Написать программу, которая запускает другую программу при помощи fork 
* и ждет ее завершения.
*/
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>

int main(){

	pid_t pid;
	int status;

	pid = fork();

	if (pid > 0){
		printf("First pid = %d, cpid = %d\n", getpid(), pid);
		wait (&status);
		printf("Next status: %d\n", status);
	}
	else if (!pid){
		printf("Second  pid = %d, pid = %d, ppid = %d\n", pid, getpid(), getppid());
		sleep(7);
		printf("The end.\n");
		return 0;
	}

	else if (pid == -1)
		perror("fork");

	return 0;
} 