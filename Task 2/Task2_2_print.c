/*
* Написать две программы, которые обмениваются через разделяемую память.
* Одна инкрементирует число, вторая печатает на экран.
* Добавить в программу изменения переменной возможность изменения 
* по срабатыванию таймера, например 1 раз в секунду.
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/types.h>

#define SM_OBJECT_NAME "shared_memory"
#define SM_OBJECT_SIZE 100

int main (int argc, char ** argv) {

    int shm;
    char *addr;

    if ( (shm = shm_open(SM_OBJECT_NAME, O_RDWR, 0777)) == -1 ) {
        perror("shm_open");
        return 1;
    }

    addr = mmap(0, SM_OBJECT_SIZE+1, PROT_WRITE|PROT_READ, MAP_SHARED, shm, 0);
    if ( addr == (char*)-1 ) {
    	perror("mmap");
    	return 1;
    }

    printf("Result: %s\n", addr);


    munmap(addr, SM_OBJECT_SIZE);
    close(shm);
} 