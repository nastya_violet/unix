/*
* 1. Сервер UDP
* печатает hello или ничего не делает
* принять команду start или stop
* 2. Посылает команду UDP
* start или stop
*/

#include <string.h>
#include <stdio.h> 
 #include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#define PORT 		1234
#define MAXLINE 	1024

int main() {
	int s_fd;
	char buf[MAXLINE];
	struct s_addr_in servaddr, c_addr;
	printf("Hello\n");

	if ( (s_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	} 

	memset(&servaddr, 0, sizeof(servaddr));
	memset(&c_ddr, 0, sizeof(cliaddr));

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(PORT);

	if ( bind(s_fd, (const struct s_addr *)&servaddr, sizeof(servaddr)) < 0 )
	{ 
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	int len, n;

	len = sizeof(c_addr);

	n = recvfrom(s_fd, (char *)buf, MAXLINE,
                MSG_WAITALL, ( struct s_addr *) &c_addr,
                &len);

	buf[n] = '\0';

	printf("Client : %s\n", buffer);
	return 0;
} 