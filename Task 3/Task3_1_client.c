/*
* 1. Сервер UDP
* печатает hello или ничего не делает
* принять команду start или stop
* 2. Посылает команду UDP
* start или stop
*/

#include <string.h>
#include <stdio.h> 
 #include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#define PORT 		1234
#define MAXLINE 	1024

int main(int argc, char ** argv) {
	int s_fd;
	char buf[MAXLINE];
	char *msg = "";
	struct s_addr_in	 servaddr;

	if (argc > 1){
		msg = argv[1];
	} else{
		printf("Using: ./client 'Start' or ./client 'Stop'\n");
		return 1;
	}

	if ( (s_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	} 

	memset(&servaddr, 0, sizeof(servaddr));

	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(PORT);
	servaddr.sin_addr.s_addr = INADDR_ANY;

	int n, len;
	sendto(s_fd, (const char *)msg, strlen(msg),
		MSG_CONFIRM, (const struct sockaddr *) &servaddr,
			sizeof(servaddr));

	printf("'%s' message was sent.\n", msg);

	close(s_fd);
	return 0;
} 